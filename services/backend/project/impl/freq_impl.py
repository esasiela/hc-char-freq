def freq_char(s: str):
    """
    Returns the least frequently occurring character found in the input string,
    plus a scrambled version of the string with characters grouped together and
    sorted by their frequency, with groups of least frequently occurring characters
    on the left.

    From the user story...

    Create an application that will prompt for a user to input a string.
    Using the user’s inputted string, find the first letter that is not repeated.

    For example: If given the string ‘Bubble’, the letter ‘u’ would be the first
    character returned from the program. Once the first character is found and
    displayed back to the user, rewrite the string in order of number of
    occurrences and order from the inputted string.

    In the above example ‘Bubble’ would then be rewritten as ‘uleBbb’.
    Display this to the user.

    Parameters:
    s (str): the input string

    Returns:
    char: The least frequently occurring character found in s
    str: The scrambled version of the string
    int: The lowest number of occurances of a character in s
    """
    if s is None:
        return None, None, None

    if s == "":
        return "", "", 0

    # 1) lowercase copy of the whole thing
    # 2) run through each char, getting the counts
    # 3) add each char by its count
    # 4) assemble the final string
    # 5) pull off the left char as the special one

    count_map = {}
    # 1) lowercase copy of the whole thing
    lc = s.lower()

    # 2) run through each char, getting the counts
    for raw_char, lc_char in zip(s, lc):
        count = lc.count(lc_char)

        if count not in count_map:
            # put an empty list in there so we can append
            count_map[count] = {}

        # 3) add each char by its count
        if lc_char not in count_map[count].keys():
            count_map[count][lc_char] = []

        count_map[count][lc_char].append(raw_char)

    # 4) assemble the final string
    final_string = ""
    least_count = len(s)
    for count in sorted(count_map.keys()):
        if count < least_count:
            least_count = count

        # the next line depends on keys() returning keys in the order they were added
        lc_chars = count_map[count].keys()

        for lc_char in lc_chars:
            # 5) special handling of upper case guys
            for raw_char in count_map[count][lc_char]:
                final_string = final_string + raw_char

    return final_string[0], final_string, least_count


if __name__ == "__main__":
    keep_going = True

    while keep_going:
        input_string = input("Input string ('q' to quit): ")

        if input_string == "q":
            keep_going = False
        else:
            least_char, scrambled, least_count = freq_char(input_string)
            non_repeat = least_char if least_count == 1 else ""
            print("First non-repeated character  :", non_repeat)
            print("First least frequent char     :", least_char)
            print("Scrambled input string        :", scrambled)
            print("Least frequent char occurances:", least_count)
            print("")

    print("exiting.")
