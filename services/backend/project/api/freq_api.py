from flask_restx import Namespace, Resource

from project.impl.freq_impl import freq_char

freq_namespace = Namespace("freq")


class Freq(Resource):
    @freq_namespace.response(200, "Success")
    @freq_namespace.response(400, "Invalid request")
    def get(self, input_string):
        """
        Runs the impl and returns the values
        """

        # this should never get here if the namespace route is set up right
        if input_string is None or input_string == 0:
            freq_namespace.abort(400, "Invalid request")

        response_object = {}

        least_char, scrambled, least_count = freq_char(input_string)

        response_object["char"] = least_char
        response_object["str"] = scrambled
        response_object["count"] = least_count

        return response_object, 200


freq_namespace.add_resource(Freq, "/<string:input_string>")
