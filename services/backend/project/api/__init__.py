from flask_restx import Api

from project.api.freq_api import freq_namespace

api = Api(version="1.0", title="Users API", doc="/doc/")

api.add_namespace(freq_namespace, path="/api/freq")
