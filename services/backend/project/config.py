import os


class BaseConfig:
    TESTING = False
    SECRET_KEY = "not_so_secret_key"


class DevelopmentConfig(BaseConfig):
    SECRET_KEY = "secret_dev_key"


class TestingConfig(BaseConfig):
    TESTING = True
    SECRET_KEY = "secret_test_key"


class ProductionConfig(BaseConfig):
    SECRET_KEY = os.getenv("SECRET_KEY", "not_so_secret_key")
