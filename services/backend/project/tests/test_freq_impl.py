from project.impl.freq_impl import freq_char


def test_impl_works():
    input_string = "Bubble"
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == "u"
    assert scrambled == "uleBbb"
    assert least_count == 1


def test_impl_works_no_duplicates():
    input_string = "Ethan"
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == "E"
    assert scrambled == "Ethan"
    assert least_count == 1


def test_impl_works_multiple_duplicates1():
    input_string = "aabbcCz"
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == "z"
    assert scrambled == "zaabbcC"
    assert least_count == 1


def test_impl_works_multiple_duplicates2():
    input_string = "aAabbccz"
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == "z"
    assert scrambled == "zbbccaAa"
    assert least_count == 1


def test_impl_works_no_singleton():
    input_string = "xxyyzz"
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == "x"
    assert scrambled == "xxyyzz"
    assert least_count == 2


def test_impl_input_none():
    input_string = None
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char is None
    assert scrambled is None
    assert least_count is None


def test_impl_input_empty():
    input_string = ""
    least_char, scrambled, least_count = freq_char(input_string)
    assert least_char == ""
    assert scrambled == ""
    assert least_count == 0
