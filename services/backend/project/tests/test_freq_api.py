import json


def test_api_works(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/Bubble")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "u" == data["char"]
    assert "uleBbb" == data["str"]
    assert 1 == data["count"]


def test_api_works_no_duplicates(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/Ethan")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "E" == data["char"]
    assert "Ethan" == data["str"]
    assert 1 == data["count"]


def test_api_works_multiple_duplicates1(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/aabbcCz")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "z" == data["char"]
    assert "zaabbcC" == data["str"]
    assert 1 == data["count"]


def test_api_works_multiple_duplicates2(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/aAabbccz")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "z" == data["char"]
    assert "zbbccaAa" == data["str"]
    assert 1 == data["count"]


def test_api_works_no_singleton(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/xxyyzz")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "x" == data["char"]
    assert "xxyyzz" == data["str"]
    assert 2 == data["count"]


def test_api_missing_string(test_app):
    client = test_app.test_client()

    resp = client.get("/api/freq/")

    assert resp.status_code == 404
