var express = require("express");
var router = express.Router();

/* TODO: move String.count() out of route and into impl */
String.prototype.count = function(lit, cis) {
  var m = this.toString().match(new RegExp(lit, cis ? "gi" : "g"));
  return m !== null ? m.length : 0;
};

/* TODO: move freq_char() out of route and into impl */
function freq_char(s) {
  /*
  Returns the least frequently occurring character found in the input string,
  plus a scrambled version of the string with characters grouped together and
  sorted by their frequency, with groups of least frequently occurring characters
  on the left.

  From the user story...

  Create an application that will prompt for a user to input a string.
  Using the user’s inputted string, find the first letter that is not repeated.

  For example: If given the string ‘Bubble’, the letter ‘u’ would be the first
  character returned from the program. Once the first character is found and
  displayed back to the user, rewrite the string in order of number of
  occurrences and order from the inputted string.

  In the above example ‘Bubble’ would then be rewritten as ‘uleBbb’.
  Display this to the user.

  Parameters:
  s (str): the input string

  Returns:
  char: The least frequently occurring character found in s
  str: The scrambled version of the string
  int: The lowest number of occurances of a character in s
  */
  if (s === null) {
    return {
      char: null,
      str: null,
      count: null
    };
  }

  if (s === "") {
    return {
      char: null,
      str: null,
      count: 0
    };
  }

  let countMap = new Map();
  let sLower = s.toLowerCase();

  let scrambled = "";
  let leastCount = s.length;

  for (let i = 0; i < sLower.length; i++) {
    let rawChar = s.charAt(i);
    let lcChar = sLower.charAt(i);

    let count = sLower.count(lcChar);

    /*
    console.log("Loop Index: ", i);
    console.log("rawChar: ", rawChar);
    console.log("lcChar: ", lcChar);
    console.log("count: ", count);
    console.log("countMap before null check: ", countMap);
    */

    if (!countMap.has(count)) {
      /* console.log("assigning an empty value for countMap[", count, "]"); */
      countMap.set(count, new Map());
    } /* else {
      console.log("NOT assigning an empty value for countMap[", count , "] ", countMap.get(count));
    }

    console.log("countMap after null check: ", countMap);
    */

    if (!countMap.get(count).has(lcChar)) {
      /* console.log("countMap[", count , "] does not have key [", lcChar, "], adding empty array"); */
      countMap.get(count).set(lcChar, []);
    }

    countMap
      .get(count)
      .get(lcChar)
      .push(rawChar);
  }

  for (let count of [...countMap.keys()].sort()) {
    if (count < leastCount) {
      leastCount = count;
    }

    let lcChars = countMap.get(count).keys();

    for (let lcChar of lcChars) {
      for (let rawChar of countMap.get(count).get(lcChar)) {
        scrambled = scrambled.concat(rawChar);
      }
    }
  }

  return {
    char: scrambled.charAt(0),
    str: scrambled,
    count: leastCount
  };
}

/* GET char freq microservice */
/* router.get("/freq/:inputString", function(req, res, next) { */
router.get("/freq/:inputString", function(req, res) {
  let inputString = req.params.inputString;

  /* this should never execute if the route is set up right */
  if (inputString === null || inputString === "") {
    res.status(400).end("Invalid request");
  }

  let responseObject = freq_char(inputString);

  /*
  let leastChar = inputString.charAt(0);
  let scrambled = inputString.split("").reverse().join("");
  let leastCount = 1;

  responseObject.char = leastChar;
  responseObject.str = scrambled;
  responseObject.count = leastCount;
  */

  res.status(200).json(responseObject);
});

module.exports = router;
