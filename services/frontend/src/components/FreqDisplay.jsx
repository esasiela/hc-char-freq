import React from "react";
import PropTypes from "prop-types";

const FreqDisplay = props => {
  return (
    <section data-testid="freqDisplay">
      <h2 className="title is-2">Results</h2>
      <div>
        <table className="table is-hoverable is-halfwidth">
          <tbody>
            <tr>
              <td>Input String:</td>
              <td className="input-string">{props.inputString}</td>
            </tr>
            <tr>
              <td>First Non-Repeated Character:</td>
              <td className="non-repeat">
                {props.leastCount == 1 && props.leastChar}
              </td>
            </tr>
            <tr>
              <td>Least Frequent Character:</td>
              <td className="least-char">{props.leastChar}</td>
            </tr>
            <tr>
              <td>Scrambled:</td>
              <td className="scrambled">{props.scrambled}</td>
            </tr>
          </tbody>
        </table>

        {props.leastCount > 1 && (
          <div className="ambiguous-requirements">
            No characters appear in the input string only once. In fact, the
            minimum frequency is {props.leastCount}.
            <br />
            <br />
            The requirements were ambiguous on handling no singletons and the
            business owner was not available for clarification. I went with
            allowing strings with a minimum character occurance value greater
            than one. Once the ambiguity is cleared up, it can be fixed and this
            message removed rather quickly.
            <br />
            <br />
            Such is the beauty of CI/CD and automated deployment.
          </div>
        )}
      </div>
    </section>
  );
};

FreqDisplay.propTypes = {
  inputString: PropTypes.string.isRequired,
  leastChar: PropTypes.string.isRequired,
  scrambled: PropTypes.string.isRequired,
  leastCount: PropTypes.number.isRequired
};

export default FreqDisplay;
