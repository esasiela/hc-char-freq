import React from "react";
import { render, cleanup } from "@testing-library/react";

import FreqInput from "../FreqInput";

afterEach(cleanup);

const props = {
  handleFreqInputSubmit: () => {
    return true;
  }
};

it("renders properly", () => {
  const { getByText } = renderWithRouter(<FreqInput {...props} />);
  expect(getByText("Input")).toHaveClass("title");
});

it("renders", () => {
  const { asFragment } = renderWithRouter(<FreqInput {...props} />);
  expect(asFragment()).toMatchSnapshot();
});
