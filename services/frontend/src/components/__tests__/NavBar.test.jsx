import React from "react";
import { cleanup, wait } from "@testing-library/react";

import NavBar from "../NavBar";

afterEach(cleanup);

describe("when not authenticated", () => {
  const props = {
    title: "Hello, World!"
  };

  it("renders the default props", async () => {
    const { getByText, findByTestId } = renderWithRouter(<NavBar {...props} />);
    /* expect(getByText(props.title)).toHaveClass("nav-title"); */
    expect((await findByTestId("nav-about")).innerHTML).toBe("About");
  });

  it("renders", () => {
    const { asFragment } = renderWithRouter(<NavBar {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
