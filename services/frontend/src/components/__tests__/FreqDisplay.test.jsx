import React from "react";
import { render, cleanup, wait } from "@testing-library/react";

import FreqDisplay from "../FreqDisplay";

afterEach(cleanup);

describe("when have data", () => {
  const props = {
    inputString: "xxyyzzZ",
    leastChar: "x",
    scrambled: "zzZxxyy",
    leastCount: 2
  };

  it("renders the data", () => {
    const { getByText, findByTestId } = renderWithRouter(
      <FreqDisplay {...props} />
    );
    expect(getByText(props.inputString)).toHaveClass("input-string");
    expect(getByText(props.leastChar)).toHaveClass("least-char");
    expect(getByText(props.scrambled)).toHaveClass("scrambled");
  });

  it("renders", () => {
    const { asFragment } = renderWithRouter(<FreqDisplay {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
