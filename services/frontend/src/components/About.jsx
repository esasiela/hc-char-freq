import React from "react";

const About = () => (
  <div>
    <h1 className="title is-1">About</h1>
    <hr />
    <br />
    <p className="content">
      This application is a demonstration of some simple string manipulation
      (i.e. business logic), plus the infrastructure for a robust application:
    </p>

    <div>
      <ol>
        <li>Python backend business logic implementation</li>
        <li>Flask API to expose business logic</li>
        <li>React.js front end client</li>
        <li>Frontend/Backend Docker containers</li>
        <li>GitLab CI/CD pipeline for build, test, deploy</li>
        <li>Heroku/nginx/gunicorn automated production deployment</li>
      </ol>
    </div>

    <hr />
    <br />

    <h2 className="title is-2">Source Repository</h2>
    <p className="repository">
      The source is in the{" "}
      <a href="https://gitlab.com/esasiela/hc-char-freq">hc-char-freq</a> GitLab
      repository.
    </p>

    <hr />
    <br />

    <h2 className="title is-2">Portfolio</h2>
    <p className="portfolio">
      For other interesting samples, visit my portfolio site on{" "}
      <a href="https://www.hedgecourt.com">Hedge Court</a>.
    </p>
  </div>
);

export default About;
