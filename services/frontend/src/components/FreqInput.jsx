import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import * as Yup from "yup";

import "./form.css";

const FreqInput = props => {
  return (
    <div>
      <h2 className="title is-2">Input</h2>
      <Formik
        initialValues={{
          inputString: ""
        }}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          props.handleFreqInputSubmit(values);
          resetForm();
          setSubmitting(false);
        }}
        /*
        validationSchema={Yup.object().shape({
          inputString: Yup.string().required("Input String is required."),
        })}
        */
      >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          /*
          <label className="label" htmlFor="input-inputString">
            Input String
          </label>
          */
          return (
            <form onSubmit={handleSubmit}>
              <div className="field">
                <input
                  name="inputString"
                  id="input-inputString"
                  className={
                    errors.inputString && touched.inputString
                      ? "input error"
                      : "input"
                  }
                  type="text"
                  placeholder="Why not try 'Bubble'?"
                  value={values.inputString}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {errors.inputString &&
                  touched.inputString(
                    <div className="input-feedback">{errors.inputString}</div>
                  )}
              </div>
            </form>
          );
        }}
      </Formik>
    </div>
  );
};

FreqInput.propTypes = {
  handleFreqInputSubmit: PropTypes.func.isRequired
};

export default FreqInput;
