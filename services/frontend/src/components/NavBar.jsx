import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import "./NavBar.css";
import logo from "./hc-logo-2020-web-charfreq.png";

const titleStyle = {
  fontWeight: "bold"
};

const NavBar = props => {
  let menu = (
    <div className="navbar-menu">
      <div className="navbar-end">
        <Link to="/about" className="navbar-item" data-testid="nav-about">
          About
        </Link>
      </div>
    </div>
  );

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <section className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item nav-title" style={titleStyle}>
            <img src={logo} alt={props.title} />
          </Link>
          <span
            className="nav-toggle navbar-burger"
            onClick={() => {
              let toggle = document.querySelector(".nav-toggle");
              let menu = document.querySelector(".navbar-menu");
              toggle.classList.toggle("is-active");
              menu.classList.toggle("is-active");
            }}
          >
            <span />
            <span />
            <span />
          </span>
        </div>
        {menu}
      </section>
    </nav>
  );
};

NavBar.propTypes = {
  title: PropTypes.string.isRequired
};

export default NavBar;
