import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Modal from "react-modal";
import axios from "axios";

import About from "./components/About";
import Message from "./components/Message";
import NavBar from "./components/NavBar";
import FreqDisplay from "./components/FreqDisplay";
import FreqInput from "./components/FreqInput";

Modal.setAppElement(document.getElementById("root"));

class App extends Component {
  constructor() {
    super();

    this.state = {
      input_string: "",
      least_char: "",
      scrambled: "",
      least_count: 0,
      title: "Hedge Court - Char Freq"
    };
  }

  componentDidMount = () => {
    console.log("app did mount");
  };

  createMessage = (type, text) => {
    this.setState({
      messageType: type,
      messageText: text
    });

    setTimeout(() => {
      this.removeMessage();
    }, 4000);
  };

  removeMessage = () => {
    this.setState({
      messageType: null,
      messageText: null
    });
  };

  handleFreqInputSubmit = data => {
    const url =
      `${process.env.REACT_APP_FREQ_SERVICE_URL}/api/freq/` + data.inputString;
    console.log("freq input submit data...");
    console.log(data);
    console.log(url);

    axios
      .get(url)
      .then(res => {
        console.log(res.data);
        this.setState({
          input_string: data.inputString,
          least_char: res.data.char,
          scrambled: res.data.str,
          least_count: res.data.count
        });
        /* this.createMessage("success", "Successfully processed string."); */
      })
      .catch(err => {
        console.log(err);
        this.createMessage("danger", "Error processing string.");
      });
  };

  render() {
    return (
      <div>
        <NavBar title={this.state.title} />
        <section className="section">
          <div className="container">
            {this.state.messageType && this.state.messageText && (
              <Message
                messageType={this.state.messageType}
                messageText={this.state.messageText}
                removeMessage={this.removeMessage}
              />
            )}
            <div className="columns">
              <div className="column is-one-quarter"></div>
              <div className="column is-half">
                <Switch>
                  <Route
                    exact
                    path="/"
                    render={() => (
                      <div>
                        <FreqInput
                          handleFreqInputSubmit={this.handleFreqInputSubmit}
                        />

                        <br />
                        <br />

                        <FreqDisplay
                          inputString={this.state.input_string}
                          leastChar={this.state.least_char}
                          scrambled={this.state.scrambled}
                          leastCount={this.state.least_count}
                        />
                      </div>
                    )}
                  />
                  <Route exact path="/about" component={About} />
                </Switch>
              </div>
              <div className="column is-one-quarter"></div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;
