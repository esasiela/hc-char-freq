# Hedge Court - Fun with Character Frequency in a String

[![pipeline status](https://gitlab.com/esasiela/hc-char-freq/badges/master/pipeline.svg)](https://gitlab.com/esasiela/hc-char-freq/commits/master)

This is the repository for a project illustrating some simple string manipulation
(i.e. business logic), plus the infrastructure for a robust application:

* Python backend business logic implementation
* Flask API to expose business logic
* React.js front end client
* Frontend/Backend Docker containers
* Heroku production deployment
* GitLab CI/CD pipeline for build, test, deploy

# Production Application

TODO - Production Link to Heroku environment

# Minimum Viable Product

The MVP for this project is located in `services/backend/project/impl/freq_impl.py`

Before adding the fun React frontend and Heroku deployment, there was the command line interface (CLI).

Steps to run it (after cloning or unzipping the repository):

```
cd services/backend
python -m venv venv
venv/Scripts/activate
pip install -r requirements-dev.txt
python project/impl/freq_impl.py
```

# Local Docker

To run the whole she-bang on a system running docker...

```
docker-compose up -d --build
```

Then visit http://localhost:3007 to access the frontend.
